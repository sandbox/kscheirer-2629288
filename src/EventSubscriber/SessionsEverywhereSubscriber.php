<?php

/**
 * @file
 * Contains \Drupal\sessions_everywhere\EventSubscriber\SessionsEverywhereSubscriber.
 */

namespace Drupal\sessions_everywhere\EventSubscriber;

// This is the interface we are going to implement.
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

// This class contains the event we want to subscribe to.
use Symfony\Component\HttpKernel\KernelEvents;

// Our event listener method will receive one of these.
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

// We'll use this to perform a redirect if necessary.
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Provides a SessionsEverywhereSubscriber.
 */
class SessionsEverywhereSubscriber implements EventSubscriberInterface {

  /**
   * // only if KernelEvents::REQUEST !!!
   * @see Symfony\Component\HttpKernel\KernelEvents for details
   *
   * @param Symfony\Component\HttpKernel\Event\GetResponseEvent $event
   *   The Event to process.
   */
  public function SessionsEverywhereLoad(GetResponseEvent $event) {
    drupal_set_message('SessionsEverywhere: subscribed');
    $request = $event->getRequest();
    $session = $request->getSession();
    $session->set('sessions_everywhere.test', 'test');
    //$event->getRequest()->getSession()->set('sessions_everywhere.test', 'test);
  }

  /**
   * {@inheritdoc}
   */
  static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = array('SessionsEverywhereLoad', 20);
    return $events;
  }
}
